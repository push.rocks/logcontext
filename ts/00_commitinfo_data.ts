/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/logcontext',
  version: '2.0.0',
  description: 'enrich logs with context'
}
